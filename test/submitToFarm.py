import os,sys
import uuid
import optparse

def RunPacked(args):
    exe,cfg=args
    os.system('./bin/%s --cfg %s'%(exe,cfg))
    return True


def main():
    """wrapper to be used from command line"""
    usage = 'usage: %prog [options]'
    parser = optparse.OptionParser(usage)
    parser.add_option('-e', '--exe',       dest='exe',         help='executable [%d]',           default='prepareCalibrationTree')
    parser.add_option('-s', '--sim',       dest='simTag',      help='simulation tag [%d]',       default='HGcal__version63_model2_BOFF')
    parser.add_option(      '--submit',    dest='subToCondor', help='submit to condor [%d]',     default=False, action='store_true')
    parser.add_option(      '--jobs',      dest='jobs',        help='parallel jobs [%d]',        default=1, type=int)
    parser.add_option('-r', '--reco',      dest='recoTag',     help='reconstruction tag [%d]',   default='DigiIC3_200u_version63_model2_BOFF')
    parser.add_option(      '--physLayers', dest='physLayers',  help='physical layers [%d]',      default=50, type=int)
    parser.add_option(      '--icsmear',    dest='icsmear',     help='additional i.calib smearing [%3.3f]',      default=-1, type=float)
    parser.add_option('-n', '--ninputs',    dest='pNinputs',    help='files per job [%d]',        default=10, type=int)
    parser.add_option('-o', '--out',        dest='outFile',     help='outFile name [%d]',         default='results/trees.root')
    (opt, args) = parser.parse_args()
    
    simFiles=[ f for f in os.listdir(args[0]) if opt.simTag in f ]
    nfiles=len(simFiles)
    nchunks=nfiles/opt.pNinputs
    if nchunks*opt.pNinputs<nfiles: nchunks+=1
    print 'Found %d simulated files to loop over, dividing in %d chunks of %d'%(nfiles,nchunks,opt.pNinputs)

    FARMDIR='FARM_%s'%uuid.uuid1().hex
    os.system('mkdir -p %s'%FARMDIR)

    task_list=[]
    for i in xrange(0,nchunks):
        pStartInputs=i*opt.pNinputs
        cfgName='%s/cfg_%d.cfg'%(FARMDIR,i)
        with open(cfgName,'w') as cfg:
            cfg.write('pNevts=0\n')
            cfg.write('pStartInputs=%d\n'%pStartInputs)
            cfg.write('pNinputs=%d\n'%opt.pNinputs)
            cfg.write('inDir=%s\n'%args[0])
            cfg.write('recoTag=%s\n'%opt.recoTag)
            cfg.write('recoTreeName=RecoTree\n')
            cfg.write('simTag=%s\n'%opt.simTag)
            cfg.write('simTreeName=HGCSSTree\n')
            out_pfix='_%d.root'%i
            if opt.icsmear>0:
                out_pfix='_ic{:d}{}'.format(int(opt.icsmear*100),out_pfix)
            cfg.write('outFile=%s\n'%(opt.outFile.replace('.root',out_pfix)))
            cfg.write('debug=0\n')
            cfg.write('enmin=1.0\n')
            if opt.exe=='prepareCalibrationTree':                
                cfg.write('dRwindow=1.0\n')
            cfg.write('pNPhysLayers={}\n'.format(opt.physLayers))
            cfg.write('icsmear={}\n'.format(opt.icsmear))

        task_list.append( (opt.exe,cfgName) )
    print('Collected {} chunks to process'.format(len(task_list)))

    if not opt.subToCondor:
        print('Running locally')
        if opt.jobs<=1:
            for args in task_list: 
                RunPacked(args)
        else:
            from multiprocessing import Pool
            pool = Pool(opt.jobs)
            pool.map(RunPacked, task_list)
    else:
        print('Submitting to condor')
        
        worker_f='%s/worker.sh'%FARMDIR
        curdir=os.path.abspath(os.curdir)
        with open(worker_f,'w') as f:
            f.write('#!/bin/bash\n')            
            f.write('#environment setup\n')
            f.write('HOME=`pwd`\n')
            f.write('source {}/../PFCalEE/g4env.sh\n'.format(curdir))
            f.write('export PYTHONDIR=/afs/cern.ch/sw/lcg/external/Python/2.7.3/$ARCH/\n')
            f.write('export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib\n')
            f.write('export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:$PYTHONDIR/lib:{0}/../PFCalEE/userlib/lib:{0}/lib\n'.format(curdir))
            f.write('#run the analysis\n')
            f.write('exe=$1\n')
            f.write('cfg=$2\n')
            f.write('${exe} --cfg ${cfg}\n')
        os.system('chmod u+x {}'.format(worker_f))

        condor_f='{}/condor.sub'.format(FARMDIR)
        with open(condor_f,'w') as condor:
            condor.write('executable={}\n'.format(os.path.abspath(worker_f)))
            condor.write('output={}/condor.out\n'.format(FARMDIR))
            condor.write('error={}/condor.err\n'.format(FARMDIR))
            condor.write('log={}/condor.log\n'.format(FARMDIR))
            condor.write('+JobFlavour=\"tomorrow\"\n')
            condor.write('request_cpus=4\n')
            for exe,cfg in task_list:
                condor.write('arguments={}/bin/{} {}\n'.format(curdir,exe,os.path.abspath(cfg)))
                condor.write('queue 1\n')
        os.system('condor_submit {}'.format(condor_f))

if __name__ == "__main__":
    sys.exit(main())
