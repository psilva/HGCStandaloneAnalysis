#ifndef __shower_selection_tools_h__ 
#define __shower_selection_tools_h__
#include "ROOT/RVec.hxx"

using namespace ROOT::VecOps; 

using rvec_f = const RVec<float>;
using rvec_i = const RVec<int>;
using rvec_b = const RVec<bool>;

/**
    @short computes the global compensation variable for a collection of hits
    elim - the threshold to be used
    min_hits - minimum number of hits
    cglob_{min,max} - limit output to these values
*/
float computeCglob(const rvec_f &hit_en,
                   float elim=5., size_t min_nhits=3,
                   float cglob_min=0., float cglob_max=2.)
{
    if(hit_en.size()<min_nhits) return -1.;
    
    float mean_hit_en = Mean(hit_en);
    int nbelow_mean( hit_en[hit_en<mean_hit_en].size() );    
    if(nbelow_mean==0) return -1.;

    int nbelow_elim( hit_en[hit_en<elim].size() );
    float cglob=float(nbelow_elim)/float(nbelow_mean);
    cglob=min(cglob_max,max(cglob_min,cglob));
    
    return cglob;
}


#endif
