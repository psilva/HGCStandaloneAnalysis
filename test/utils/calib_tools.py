import scipy.stats
from scipy.optimize import curve_fit
import numpy as np

def defineCalibrationGrid(df,et_pts=[2,5,7,10,15,25,35,50,75,100,150,200,300,500],eta_pts=[1.7,2.0,2.4]):
    
    """ 
    Defines the list of simulated points (avoids nummerical approx. errors) 
    and the corresponding masks to obtain the slice.
    It returns two dicts where the key is (Et,Eta) of the shower and the content is:
    - the number of entries (sim_pts) 
    - the masks (sim_masks)
    """
    
    df['et_cat']=0
    for i_et, et in enumerate(et_pts):
        etMask = (np.abs((df['genEt']/et)-1)<0.1)
        df['et_cat']=np.where(etMask,i_et+1,df['et_cat'])
        
    df['eta_cat']=0
    for i_eta, eta in enumerate(eta_pts):         
        etaMask=(np.abs(df['genEta']-eta)<0.1)
        df['eta_cat'] = np.where(etaMask,i_eta+1,df['eta_cat'])

    #return unmatched points
    mask=(df['et_cat']>0) & (df['eta_cat']>0)
    df=df[ mask ]

    return df


def customFermiFunction(x,delta_p,delta_m,lambd,x0):
    
    """
    customized fermi function
    $y = \delta_m + (\delta_p-\delta_m) / 1 + \exp^{-\lambda(x-x_0)}$
    """
    
    return delta_m + (delta_p-delta_m)/(1.0+np.exp( -lambd*(x-x0) ) )


def pol2LogX(x,a,b,c):
    
    """
    2nd order polynominal in log(x)
    """
    
    lx=np.log(x)
    
    return a*lx*lx+b*lx+c


def gaussian_core(x,*p):
    
    """just a gaussian function..."""
    
    k, mu, sigma = p
    return k*np.exp(-0.5*(((x-mu)/sigma)**2))


def getMedianAndEffSigma(x):
    
    """
    estimates the median and the quantiles for a 68%CI around it
    returns the median, median uncertainty, effective width and the unc. on eff. width
    the uncertainties are compued using a gaussian approximation
    (a la ROOT https://root.cern.ch/doc/master/TH1_8cxx_source.html#l07467)
    """
    
    p=np.percentile(x, [16,50,84])
    sigma=np.std(x,axis=0)
    n=x.shape[0]
    
    #median and uncertainty on median
    r=p[1]
    unc_r=1.253*sigma/np.sqrt(n)
    
    #eff. width and uncertainty on it 
    wid_r=0.5*(p[2]-p[0])
    unc_wid_r=sigma/np.sqrt(2*n)
    
    return [r,unc_r,wid_r,unc_wid_r]


def getGaussianCoreParameters(x,xbins):
    
    """
    fits the gaussian core using only the values within effective sigma (68% CI)
    returns the fitted parameters and their errors
    """
    
    median,median_unc,sigma_eff,sigma_eff_unc=getMedianAndEffSigma(x)
    
    #bin and fit
    cts,bins=np.histogram(x, bins=xbins)    
    p0 = [np.sum(cts), median, sigma_eff]
    bin_centres = (bins[:-1] + bins[1:])/2
    fit_mask=(bin_centres>median-sigma_eff) & (bin_centres<median+sigma_eff)
    nevts=cts[fit_mask].sum()

    if nevts.sum()>50:
        try:
            maxfev=500*(nevts+1)
            popt, covar = curve_fit(gaussian_core, bin_centres[fit_mask], cts[fit_mask], p0=p0,maxfev=maxfev)
            return popt.tolist() + np.sqrt(np.diagonal(covar)).tolist() + [True]
        except Exception as e:
            print(e)
            pass

    print('Found too few events to fit {}, or fit failed, returning effective values'.format(nevts))
    return [nevts,median,sigma_eff,np.sqrt(nevts),median_unc,sigma_eff_unc,False]
        
def resol2_func_ix(x, a, b, c):
    return (a**2)*x + (b*x)**2 +c**2

def resol_func(x, a, b, c):
    return np.sqrt((a/np.sqrt(x))**2+(b/x)**2+c**2)
    

