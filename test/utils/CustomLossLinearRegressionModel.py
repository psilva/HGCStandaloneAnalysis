import numpy as np
from scipy.optimize import minimize
from sklearn.model_selection import KFold
from sklearn.metrics import mean_squared_error
import pandas as pd

def quantile_loss_generator(q):
    """generator for a q-quantile loss"""
    def aux_fun(y_true, y_pred):
        e = (y_true - y_pred)
        aux = np.where(np.greater_equal(e, 0.), q*e, (q-1)*e)
        return np.mean(aux)
    qkey='%3.2f'%q
    aux_fun.__name__ = 'quantile_loss_'+qkey.replace(".", "")
    return aux_fun


class CustomLossLinearRegressionModel:
    """
    Linear model with customized input loss function
    L2 regularization allowed with regularization parameter
    based on https://alex.miller.im/posts/linear-model-custom-loss-function-regularization-python/
    """
    def __init__(self, 
                 X,Y,
                 X0=None,
                 beta_init=None, 
                 loss_function=mean_squared_error,
                 regularization=0.0):
        
        self.X=X
        self.Y=Y
        if X0 is None:
            self.X0=np.zeros_like(X0.shape[0])
        else:
            self.X0=X0
        self.loss_function = loss_function
        if beta_init is None:
            self.beta_init = np.array([1.]*self.X.shape[1])
        else:
            self.beta_init = np.array(beta_init)
        self.beta = self.beta_init.copy()           
        self.regularization = regularization

    def predict(self, X, X0=None):
        prediction = np.matmul(X, self.beta)
        if not X0 is None: prediction += X0
        return(prediction)

        
    def model_error(self):
        
        """calls the custom loss/error function"""

        Ypred=self.predict(self.X,self.X0)       
        loss=self.loss_function(Ypred, self.Y)
        return loss
    
    def l2_regularized_loss(self, beta):

        """the loss function"""

        self.beta=beta
        
        #conmpute error with custom function
        err=self.model_error()         
        
        #add regularizaiton term, if required
        if self.regularization>0 :
            err += sum(self.regularization*np.array(self.beta)**2)
            
        return err
            
    def fit(self,min_opts={'maxiter': 100}):

        """runs the fit procedure"""
        
        res = minimize(self.l2_regularized_loss, 
                       self.beta_init,
                       method='BFGS', 
                       options=min_opts) 
        
        setattr(self,'minimize_results',res)
        self.beta = res.x

        
class CustomLossLinearRegressionModelCV:
    
    """
    Cross validates custom linear regression
    """
    
    def __init__(self, lambda_scan=np.logspace(-3,3,20),num_folds=5):   
        
        """
        lambda_scan - the list of regularization parameters to scan
        num_folds - the number of folds to use for CV at each step
        """
        
        self.lambda_scan=lambda_scan
        self.num_folds=num_folds
        
    def cross_validate(self,
                       X,Y,
                       X0=None,
                       beta_init=None, 
                       loss_function=mean_squared_error,
                       min_opts={'maxiter': 50}):
        
        """
        runs cross validation for each parameter value to test
        """
        
        cv_scores = []
                
        for lam in self.lambda_scan:
                        
            # Split data into training/test sets
            kf = KFold(n_splits=self.num_folds, shuffle=True)
            kf.get_n_splits(X)
            
            # Keep track of the error for each holdout fold
            k_fold_scores = []
            k_fold_train_scores=[]
            
            # Iterate over folds, using k-1 folds for training
            # and the k-th fold for validation
            for train_index, test_index in kf.split(X):
                
                # Training data
                train_X = X[train_index,:]
                train_X0 = None if X0 is None else X0[train_index]
                train_Y = Y[train_index]                
                lambda_fold_model = CustomLossLinearRegressionModel(train_X, 
                                                                    train_Y, 
                                                                    train_X0,
                                                                    beta_init, 
                                                                    loss_function, 
                                                                    lam)
                lambda_fold_model.fit(min_opts=min_opts)
                
                #check convergence
                if not lambda_fold_model.minimize_results['success']:
                    print('[Warning] fit failed at lamb={:3.3f} with opts={}'.format(lam,min_opts))
                    continue
                    
                train_Ypred = lambda_fold_model.predict(train_X,train_X0)
                train_loss =  lambda_fold_model.loss_function(train_Ypred,train_Y)                
                k_fold_train_scores.append(train_loss)
        
                #update make convergence faster next fold
                beta_init = lambda_fold_model.beta
                
                # Calculate test error
                test_X  = X[test_index,:]
                test_X0 = None if X0 is None else X0[test_index]
                test_Y  = Y[test_index]
                fold_Ypred = lambda_fold_model.predict(test_X,test_X0)
                fold_loss =  lambda_fold_model.loss_function(fold_Ypred,test_Y)                
                k_fold_scores.append(fold_loss)
            
            
            # error associated with each lambda is the average of the errors across the k folds
            lambda_mean_scores = np.mean(k_fold_scores)
            lambda_std_scores = np.std(k_fold_scores)
            lambda_mean_train_scores = np.mean(k_fold_train_scores)
            lambda_std_train_scores = np.std(k_fold_train_scores)
            
            
            cv_scores.append( [lam,
                               lambda_mean_scores,lambda_std_scores,
                               lambda_mean_train_scores,lambda_std_train_scores] )
        
        # Optimal lambda is that which minimizes the cross-validation error
        setattr(self,
                'cv_scores',
                pd.DataFrame(cv_scores,
                             columns=['lambda','test_score','test_score_std','train_score','train_score_std'])
               )

        #train the the best model and return it
        idx=self.cv_scores.idxmin()
        setattr(self,'best_lam',self.cv_scores.iloc[idx]['lambda'].iloc[0])
        print('Training best model with lambda={}'.format(self.best_lam))
        model = CustomLossLinearRegressionModel(X=X,Y=Y,X0=X0,
                                                beta_init=beta_init, 
                                                loss_function=loss_function, 
                                                regularization=self.best_lam)
        model.fit(min_opts=min_opts)
        return model
