import time
import torch 
import torch.nn as tnn 
import torch.nn.functional as F
from torch.utils.data import DataLoader,TensorDataset
from torch.optim.lr_scheduler import ExponentialLR
import numpy as np


class SemiParametricModel(tnn.Module):
    
    def __init__(self, nfeat, nparamfeat, npopt, callback, layers, dropout=0.5, activation=tnn.ReLU):
        """
        This class implements a semi-parametric regression ANN based on pytorch
        It is assumed that we are regressing a ratio of the form 
    
        $y = target / y0$
    
        where target is the final value to be predicted (e.g. an energy)
        The target will be approximated as
    
        $target = y0 + \sum f_i(x) \tilde(x)$
    
        where x=[y0,....] is a vector of global features,  $\tilde{x}$ is a vector of local features, 
        and $f_i(x)$ is a functional form, function of global features.
    
        The network will make use of connected layers to predict the parameters of the functional forms $f_i$
        and will return the ratio 
    
        $[y0 + \sum f_i(x) \tilde(x)] / y0$
    
        as the final result.

        The input variables are the following:
        nfeat - number of global features
        nparamfeat - number of local features
        npopt - number of parameters of the functional form
        callback - callback implementing the functional form
        layers - array of number of neurons in the connected layers
        dropout - dropout fraction
        activation - activation function
        """
        
        #call parent __init__
        super().__init__()
        
        self.nfeat = nfeat-nparamfeat
        self.nparamfeat = nparamfeat
        self.dropout = min(dropout,1)
        self.batchnorm = tnn.BatchNorm1d(self.nfeat)
        self.npopt = npopt
        self.callback = callback
                
        # define the layer structure
        layerlist = []
        for i,nout in enumerate(layers):
            nin=self.nfeat if i==0 else layers[i-1]
            layerlist.append(tnn.Linear(nin,nout))
            layerlist.append( activation(inplace=True) )
            layerlist.append(tnn.BatchNorm1d(nout))
            if self.dropout>0 : layerlist.append(tnn.Dropout(self.dropout))

        #last layer will produce the required outputs
        layerlist.append(tnn.Linear(layers[-1],nparamfeat*npopt))
        
        #list of layers is converted to a Sequential model as an attribute
        self.layers = tnn.Sequential(*layerlist)
        
        
    def forward(self, x):
        
        """
        performs the forward pass of the model
        """
        
        # extract embedding values from the incoming categorical data and perform dropout
        
        xvar=x[:,0]
        xfeat=x[:,:-self.nparamfeat]
        xparamfeat=x[:,-self.nparamfeat:]
        
        # normalize the incoming continuous data          
        # and evaluate the sequential part of the model
        x = self.batchnorm(xfeat)
        x = self.layers(x)
            
        #now use the predictions for the parameters of the functional form to compute the 
        #final
        delta=[]
        for i in range(self.nparamfeat):
            
            i1=i*self.npopt
            i2=(i+1)*self.npopt
            
            xi=x[:,i1:i2]            
            
            #split the tensor per columns so it can be passed as an array of arrays of parameters for the callback
            # [ [a,b,c,...], [d,e,f,...], ... ] 
            #  -> [ [[a],[d],...], [[b],[e],...], [[c],[f],...], ... ] 
            #  -> [[a,d,...], [b,e,...], [c,f,...], ...]
            xi=torch.split(xi,1,dim=1)
            popt=[ c.flatten() for c in xi ]
            fval=self.callback(xvar,*popt)
            xparamfeati=xparamfeat[:,i]
            delta.append( fval*xparamfeati) 
        
        delta = torch.stack(delta)
        delta = delta.sum(dim=0)

        return (xvar+delta)/xvar
        
    
    
def train_validate_semiparametric(X,y,nparamfeat,npopt,callback,
                                  fsplit=0.8,epochs=10,batch_size=1024,lr=1e-3,
                                  layers=[256,128,64,32],dropout=0.5,activation=tnn.ReLU,
                                  seed=41,
                                  root='./',
                                  tag=''):

    """
    train and validate the semiparametric model
    X,y - features and target
    nparamfeat - number of features for which the paramterization will be derived
    npopt - number of parameters in in the callback function 
    callback - callback function which implements the parameterization form
    fsplit - splitting fraction for training / (1-fsplit) for test
    epochs - number of epochs to train
    batch - batch size
    lr - learning rate
    layers - number of nodes in the layers
    dropout - fraction to dropout
    activation - activation function
    seed - random number generator seed
    root - base directory where to store model and log file
    tag - a tag for this training
    """
    
    #use a fixed seed for reproducibility
    torch.manual_seed(seed)
    
    #start the model
    model= SemiParametricModel(nfeat=X.shape[1],nparamfeat=nparamfeat,
                               npopt=npopt,callback=callback,
                               layers=layers,dropout=dropout,activation=activation)
    
    #if cuda is available turn to CUDA objects
    if torch.cuda.is_available():
        print("Using CUDA as it's available")
        X=X.cuda()
        y=y.cuda()
        model=model.cuda()
    
    modelurl=f'{root}/semiparametric_model{tag}.torch'
    
    #split into train, test
    fsplit = min(max(0.,fsplit),1.) #check range
    b = X.shape[0]        #full batch
    t = int((1-fsplit)*b) #test 
    train_tensors = TensorDataset(X[:b-t],y[:b-t])
    test_tensors = TensorDataset(X[b-t:b],y[b-t:b])    
    train_dataloader = DataLoader(train_tensors, batch_size=batch_size, shuffle=True)
    test_dataloader = DataLoader(test_tensors, batch_size=batch_size, shuffle=True)
    
    #start the optimizer
    optim_args={'params':model.parameters(),'lr':lr}
    optimizer = torch.optim.Adam(**optim_args)
    
    #scheduler to update the learning rate each epoch
    scheduler = ExponentialLR(optimizer, gamma=0.9)
    
    #loss function : this will be converted to RMSE later
    criterion = tnn.MSELoss()
    
    #prepare a file to store the train losses
    trainlogurl=f'{root}/semiparametric_train{tag}.log'
    with open(trainlogurl,'w') as f:
        f.write('epoch\tbatch\ttrain\tloss\n')
    def _updateTrainLog(epoch,batch,istrain,loss):
        with open(trainlogurl,'a') as f:
            f.write(f'{epoch:d} {batch:d} {istrain} {loss:3.4f}\n')
            
    #start the training
    starttime = time.time()  
    ntrainbatches=int((b-t)/batch_size)
    nnoimprov=0
    min_avg_test_loss=None
    for i in range(epochs):

        # Run the training batches
        batch_train_losses=[]
        for ibatch, (xtrain, ytrain) in enumerate(train_dataloader):
            
            # Apply the model
            ypred = model(xtrain).reshape(-1,1)
            loss = criterion(ypred, ytrain)
            batch_train_losses.append( np.sqrt(loss.item()))
            _updateTrainLog(i,ibatch,True,batch_train_losses[-1])
            
            # Update parameters
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            
            if ibatch%100==0:
                print(f'epoch {i} batch {ibatch}/{ntrainbatches} loss={batch_train_losses[-1]:.3f}')
                
        #update learning rate
        scheduler.step()
    
        #run the testing batches
        batch_test_losses = []
        with torch.no_grad():
             
            for b, (xtest, ytest) in enumerate(test_dataloader):

                # Apply the model
                ypredtest = model(xtest).reshape(-1,1)

                # Update test loss & accuracy for the epoch
                loss = criterion(ypredtest, ytest)
                batch_test_losses.append(np.sqrt(loss.item()))
                _updateTrainLog(i,b,False,batch_test_losses[-1])
        
        #report status
        avg_train_loss=np.mean(batch_train_losses)
        avg_test_loss=np.mean(batch_test_losses)
        print(f"epoch {i} average train/test loss is: {avg_train_loss:.3f}/{avg_test_loss:.3f}")
        if min_avg_test_loss is None or avg_test_loss<min_avg_test_loss:
            print(f'average test loss has improved, saving model')
            min_avg_test_loss=avg_test_loss
            nnoimprov=0
            torch.save(model, modelurl)
        else:
            nnoimprov+=1
            print(f'average test loss did not improve after {nnoimprov} epochs')
            if nnoimprov>5:
                print(f'\t => stop training')
                break
            
    #log results location
    endtime = time.time()
    print(f'Best model stored @ {modelurl}')
    print(f'Test/train losses stored @ {trainlogurl}')
    print(f'Average test loss achieved {min_avg_test_loss:.3f}')
    print(f'Total elapsed time:{(endtime-starttime)/60:3.2f} min')
    
    #return best model url, and log url
    return modelurl,trainlogurl


def predict_semiparametric(X,model,features,tag='en_l1reg'):

    """
    runs the model predicition and updates the dataframe with the result
    df - dataframe
    model - semi-parametric model
    tag - name of the final column
    """
    
    #eval in small batches
    eval_tensors = TensorDataset(X)
    eval_dataloader = DataLoader(eval_tensors, batch_size=1024, shuffle=False)
    
    
    #turn off dropout, batchnorm etc.
    model.eval()
    
    R=[]
    with torch.no_grad():
        for ibatch, xeval in enumerate(eval_dataloader):
            print(x.shape)
            R.append( model.forward(xeval).numpy() )
        
    return R
        

    
    with torch.no_grad():

        def _predict(row):
        
            #convert the features to a tensor and run the prediction
            x=[[row[c] for c in features ]]
            x=torch.tensor(x, dtype=torch.float)
        
            k=model.forward(x).item()
            
            #return the corrected initial estimate
            return row[features[0]]*k    

        #change the behavior of the BatchNorm layer to use the 
        #running estimates instead of calculating them for the current batch. 
        model.eval() 
    
        #run the predictions row-by-row (keeps memory footprint small at the cost of CPU time...)
        df[tag] = df[features].apply( _predict, axis=1 )
    
    return df