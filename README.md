# Analysis scripts for calibration

[[_TOC_]]

## Setup

Note: this is only needed in case you need to produce summary ntuples starting from standalone outputs.
If that's not the case  you can jump this section.

It is assumed that you have already installed the standalone HGCAL simulation package following [its instructions](https://github.com/pfs/PFCal).
Then do the following:

```
cd PFCal
git clone https://gitlab.cern.ch/psilva/HGCStandaloneAnalysis.git
cd HGCStandaloneAnalysis
export PYTHONDIR=/afs/cern.ch/sw/lcg/external/Python/2.7.3/$ARCH/
export PYTHONPATH=$PYTHONPATH:$ROOTSYS/lib
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$ROOTSYS/lib:$PYTHONDIR/lib:`pwd`/lib
```

To compile it suffices to type `make`

### Preparing a summary tuple for energy scale/resolution studies

A skimmer of the events generated with standalone is used to reduce the information to the bare minimum to perform studies.
To run the skimmer you can do the following:

```
./bin/prepareCalibrationTree --cfg cfg/calibrationTree.cfg
```

the configuration file is expected to contain several parameters including the number of events, the input directory,
the name of the SIM and DIGI files in the standalone production, the name of the output file.
The above example is expected to run out of the box and produce a ROOT file with the following content

* 3 histograms with the dE/dx / x0 -rad.length / lambda - int. length of the absorbers in each layer
* an ntuple with the following summary of the hits in each event

|Variable|Content|Type|
|--------|-------|-----|
|genId|generator-level pdgId| int |
|gen{en,Et,Eta,Phi}|generator-level energy, transverse energy, pseudo-rapidity, az. angle| float |
|nhits| number of rec hits | int |
|hit_si| flags if its a scintillator hit| arr. bool |
|hit_lay| layer in which the hit was recorded | arr. int |
|hit_{x,y,z}| hit coordinates | arr. float |
|hit_{men,en,endens} | hit {energy [MIP], energy [MeV], energy density [MeV]} | arr. float |
|hit_{dR,dRho} | delta R(eta,phi) or delta Rho(x,y) to the expected axis of the shower | arr.float |
|{si,sci}_sumen | energy sum of hits in scintillator and silicon per layer [MeV] | arr.float |
|total_sim_men | similar but at simulation level [MeV] | arr. float |
|avg_{em,had}Frac | average e.m./hadronic energy faction (sim. level) | arr. float |
|{si,sci}_clustsumen | jet-clustered energy with different radii in scintillator or silicon, per layer [MeV] | 2D arr. float |

To automatize the command above you can use the following wrapper instead:

```
outDir=/eos/cms/store/cmst3/user/psilva/HGCal/scenario13
version=73
particle=pi-
mkdir -p ${outDir};

python test/submitToFarm.py \
       -e prepareCalibrationTree \
       --out ${outDir}/Geant4_${particle}_version${version}.root \
       --sim HGcal__version${version}_model2_B \
       --reco DigiIC3_200u_version${version}_model2_B \
       --physLayers 26 \
       -n 10 \
       --submit \
       /eos/cms/store/cmst3/group/hgcal/Geant4/gitV08-08-00/${particle}/
```

In the above `-n` specifies the number of files per job and `--jobs` the number of threads to use in parallel executions.
Jobs can also be run locally adding `--jobs 8` to the command line.


## Energy scale/resolution studies

At this point the analysis makes use of notebooks which can be run in SWAN.
If you don't need to create the ROOT ntuples described previously than you can jump directly 
and started clicking here:

[![SWAN](https://swanserver.web.cern.ch/swanserver/images/badge_swan_white_150.png)](https://cern.ch/swanserver/cgi-bin/go/?projurl=https://gitlab.cern.ch/psilva/HGCStandaloneAnalysis.git)

* `PrepareShowerSummaries.ipynb` - this notebook loops over the ROOT ntuples and performs energy sums and related estimations and saves the final result in pandas DataFrames.
* `ShowerCalibrationSequence.ipynb` - this notebook uses the final DataFrames to study the resolution of the showers applying different methods
* `PlotCalibrationSummary.ipynb` - this notebook can be used to obtain the final collection of plots based on the outputs stored by the previous one
